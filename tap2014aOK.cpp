/*
//PROBLEMA "a" del TAP 2014

Entrada: cantidad de manos repartidas, cantidad de cartas
Salida: B o M según si el orden es correcto o incorrecto
Auxiliar: orden = B

p1. Leer (cantidad de manos repartidas)
p2. Hacer (cantidad de manos repartidas) veces
	p2.1. Leer (cantidad de cartas)
	p2.2. Hacer (cantidad de cartas) veces
		p2.2.1. 
		Si (el número de ubicación de la carta es par) entonces
			Almacenar valor y palo en variables A y B
		sino
			Almacenar valor y palo en variables C y D
		Si (hay al menos 2 cartas) entonces
			Si (Variable A es igual a variable C o variable B es igual a variable D) entonces
				orden <-- "M"
	Escribir (orden)
	orden <-- "B"
	Reinicializar en 0 variables A,B,C Y D.
*/


#include <iostream>
#include <math.h>
using namespace std;
int main()
{
	int casosDeTesteo, cantCartas;

	cin >>casosDeTesteo;

	int i, j;
	int v1, v2;
	char p1, p2;
	char orden;

	for(j=0;j<casosDeTesteo;j++)
	{
		cin >> cantCartas;
		orden = 'B';
		v1 = 0;
		v2 = 0;
		for(i=0;i<cantCartas;i++)
		{
			if(!(i%2))
			{
				cin >> v1;
				cin >> p1;
			}
			else
			{
				cin >> v2;
				cin >> p2;
			}
			if((i>0) && (v1 != 0) && (v2 != 0))
			{
				if((v1 == v2) || (p1 == p2))
				{
					orden = 'M';
				}	
			}
		}
		cout << orden << endl;
	}
	return 0;
}